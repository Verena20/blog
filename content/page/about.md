---
title: About me
subtitle: 
comments: false
---

Mijn naam is Verena van den Hoven. Ik ben 18 jaar en zit momenteel in klas CMD1A op de Hogeschool Rotterdam. Als hobby blog ik graag op mijn blog [www.inspirationtolive.nl](https://www.inspirationtolive.nl) & Instagram ik graag op mijn account [www.instagram.com/inspirationtolive](https://instagram.com/inspirationtolive). Voor mijn studie heb ik 6 jaar op het VWO op de GSR in Rotterdam gezeten.
