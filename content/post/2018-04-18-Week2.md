---
title: Design Challenge 4 - Week 2
date: 2018-04-18
---

# Maandag 16 April  
Ik heb met Bram geholpen de planning van Marleen uitgebreid uit te werken. Toen is er onderzoek gedaan naar slaap in de vorm van deskresearch.

# Woensdag 18 april  
Er is een onderzoeksplan opgesteld door het hele team. Hiervoor hebben we weer gekeken naar het onderzoeksplan van het vorige kwartaal. De plan van aanpak moest worden gepresenteerd aan Jantien en Robin. Hierbij ging een ander team ons ook beoordelen op hoe je bijvoorbeeld praat of beweegt. Ik gaf deze presentatie met Adriana en Bram. Ik kreeg als feedback dat ik nog wat enthousiaster mag presenteren. Voor ons onderzoek moeten we bedenken hoe het haalbaar wordt om de gebruiker meerdere malen mee te nemen in het ontwerpproces.
