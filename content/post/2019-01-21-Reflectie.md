---
title: Design Challenge 5B - Reflectie
date: 2019-01-21
---

# Over het kwartaal
Kwartaal 2 was wat mij betreft niet heel lekker. Doordat het niet mogelijk was om onze concepten en prototypes bij Woonstad te testen, verloren we de motivatie om nog te werken voor Woonstad. We wisten niet hoe we de doelgroep anders konden benaderen / hoe we anders onze ideeën zouden kunnen testen vanwege de taalbarrière. Aan het begin van kwartaal 2 vonden we een manier om toch nog verder te onderzoeken en te concepten, namelijk door scenario’s bij volwassenen en kleuters te testen. Hierna bleef het project helaas een beetje stil liggen, werd er tijdens de studio niet zo veel meer gedaan en had ik ook niet zoveel zin meer om wat te doen. Gelukkig had ik wel leuke uitkomsten van mijn kleuteronderzoek, waardoor ik mijn teamgenoten weer kon enthousiasmeren om een tweede prototype naast de iconen te maken, namelijk de AR Bril. Daarom zijn we de laatste weken toch nog goed bezig geweest.

De samenwerking rondom het ontwerpverslag ging niet goed. Er was nog niet veel voor gedaan, taken werden niet goed opgepakt (zo duurde het heel lang voor iets af was / was iemand met iets anders bezig tijdens de studio, wat een slechte indruk geeft / werden taken maar half aangepakt). Omdat ik er wat vaart achter wilde, heb ik daarom zelf maar een hoop dingen opgepakt, zodat ik zeker wist dat dit af zou zijn. Ook heb ik Sico aardig moeten begeleiden in het maken van het ontwerpverslag. Ik wilde eigenlijk mijn leidersrol waarbij ik onder andere taken uitdeelde loslaten dit kwartaal, maar toen ik dit deed, gebeurde er ineens vrij weinig. Daarom heb ik toch weer de leidersrol op me genomen en taken uitgedeeld. Toen ging het weer goed!

# Leerdoelen voor het team
- Studiotijd beter benutten.
- Rollen zodanig verdelen, dat dit ook voor iedereen lukt/uitkomt.
- Tegen elkaar durven zeggen als iets je niet zint (bijvoorbeeld als een taak van iemand onnodig veel tijd in beslag neemt).
- Betere afspraken en communicatie, zodat iedereen weet hoe we er voor staan, wie wat doet en wat er nog moet gebeuren.

# Leerdoelen voor mij
- Een vaste ontwerpstijl maken.
- Experimenteren en durven.
- Duidelijk laten weten aan mijn teamgenoten wat ik doe, zodat ik niet de indruk wek dat ik andere dingen zit te doen (terwijl ik gewoon voor het project bezig ben).
- Verder verdiepen in verschillende rollen van design.
