---
title: Design Challenge 1 - Introweek
date: 2017-09-01
---

In de week van 28 augustus t/m 1 september hadden we onze eerste ‘proefweek’. Hierbij leerden we de CMD-studenten kennen, vormden we ons eerste groepje en werden er uiteindelijk teams van 5 gevormd. Ik ben samen in een groepje met Sanne, Ashana, Bas en Eva terecht gekomen. Op donderdag 27 augustus hebben we een mascotte gemaakt en gepresenteerd. Ook hebben we een teamnaam, team Henkie, bedacht. 