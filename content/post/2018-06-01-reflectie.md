---
title: Design Challenge 4 - Reflectie
date: 2018-06-02
---

Ik heb het erg naar mijn zin gehad in het team! We hebben goed samengewerkt. Ik heb mijzelf als ontwerper dit en vorig kwartaal goed weten te ontwikkelen. Zo ben ik steeds beter STARRTs gaan schrijven. Ook ben ik veel beter geworden in alle competenties.

## 

1. Professionaliseren: ik heb veel boeken gelezen, zodat ik meer kennis kon toepassen. Hierdoor kreeg ik passendere concepten.

## 

2. Empathie: ik heb mij beter in de gebruiker kunnen inleven door hem vaak mee te nemen in het ontwerpproces.

## 

3. Samenwerken: ik heb geleerd om beter te samenwerken met mijn team. Zo kon ik heel goed opschieten met Adriana.

## 

4. Inrichten ontwerpproces: ik heb geleerd hoe ik mijn ontwerpproces kan inrichten en wat bepaalde acties voor invloed hebben op het ontwerpproces.

## 

5. Onderzoeken: ik heb veel nieuwe onderzoeksmethoden geleerd.

## 

6. Ideevorming: ik heb veel creatieve technieken geleerd en toegepast om ideeën te bedenken.

## 

7. Verbeelden en uitwerken: ik heb mijzelf hard uitgedaagd in deze competenties door veel visuals te maken en hier telkens feedback op te vragen.

## 

8. Evalueren van ontwerpresultaten: in kwartaal 4 heb ik de gebruiker actief betrokken in het ontwerpproces, door telkens ontwerpen te testen. Hierdoor heb ik een passend ontwerp kunnen maken.

## 

  

## 

Ik kijk uit naar jaar 2, ik hoop hier nog veel meer te kunnen leren!
