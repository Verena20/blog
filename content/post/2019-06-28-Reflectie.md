---
title: Design Challenge 6B - Reflectie

date: 2019-06-28
---

# Terugblik kwartaal 4
Afgelopen kwartaal was er weer een goede samenwerking met mijn team. Er is veel gebeurd qua beroepsproducten en we hebben een hoop vast kunnen leggen. Wat dat betreft ben ik ook zeker trots op ons werk. Wel merkte ik de laatste 3 weken van het kwartaal, dat sommige teamleden wat in de “vakantiemodus” kwamen en weinig zin meer hadden in school. Hierdoor moest ik veel aan het team trekken, zodat er gebeurde wat er moest gebeuren. Door de “vakantiemodus” miste sommige teamleden ook mededelingen, waardoor niet altijd alle deadlines werden nagekomen. Het was een beetje jammer dat het zo ging, maar ik kan het wel begrijpen dat je focus houden tot het einde van het jaar vrij lastig kan zijn. Gelukkig heeft het team en onze producten hier niet onder geleden.Ik merk aan mijzelf dat ik soms nog steeds onzeker ben over mijn visuals, daarom wil ik zelfstandiger gaan werken aan mijn visuals in mijn stage. Ik heb hier al aan gewerkt door mij weer te ontwikkelen in visual design, maar ik vraag toch nog te vaak om bevestiging. Ook durf ik niet goed te zeggen als iets niet goed gaat in het team, daar wil ik dan ook aan werken volgend schooljaar. Tot slot wil ik meer interesse tonen in visies en meningen van mijn teamgenoten, door consensus in mijn mening/visie en de mening/visie van mijn teamgenoten te vinden.

# Leerdoelen voor mijn team
- Duidelijker naar elkaar communiceren (in studio en whatsapp)
- Goed opletten als er iets gezegd wordt, zodat iedereen op de hoogte is van wat er moet gebeuren.
- Gemotiveerd blijven in lastige situaties.# Leerdoelen voor mij
- Uit comfortzone komen.
- Durven te zeggen als iets niet goed gaat, zodat de samenwerking verbetert kan worden.
- Interesse tonen in andere meningen/visies en navragen of deze mening/visie ook door anderen klasgenoten is gevonden.