---
title: Design Challenge 5A - Reflectie
date: 2018-10-31
---

# Reflectie kwartaal 5
Het kostte even wat moeite om op gang te komen dit kwartaal, na zo’n lange vakantie. Daarom verliep de samenwerking in team No Hard Feelings op het begin van het kwartaal niet helemaal soepel. Zo hadden we nog veel moeite met een taakverdeling, planning en deadlines, wat goed te zien was tijdens onze eerste deadline: de Discover poster. Er waren toen nog veel beroepsproducten niet af, waardoor het maken van de poster veel meer tijd kostte dan ik verwachtte. Op dat moment nam ik even de leiding op me en zette ik meteen taken uit voor mijn teamgenoten, waardoor we uiteindelijk de poster (nog net) op tijd konden inleveren. Maar van dit soort fouten leer je! Daarom pakken we het nu heel anders aan. We beginnen eerder op tijd en houden goed onze LAB opdrachten in de gaten. Als ik, als rol van visual designer, dingen nodig heb van mijn teamgenoten, vraag ik het meteen en zet ik taken uit. Gelukkig hebben mijn teamgenoten dit als een fijne houvast ervaren, omdat ze zo precies wisten wat ze moesten doen. Wel zat ik af en toe iets teveel in de rol van de teamleider. Daarom wil Tyoni volgend kwartaal voor zichzelf bedenken wat hij wil leiden, zodat er voor de rest een of meerdere leiders kunnen zijn. Zo hoop ik op een nog betere samenwerking in kwartaal 6!

Wel ben ik heel trots op het feit dat we zo voor onze doelgroep zijn opgekomen dit kwartaal. Al toonde Woonstad niet de meeste interesse in onze specifieke doelgroep, toch willen we hen helpen met een oplossing! Liever vragen om vergeving, dan vragen om toestemming! We hebben heel duidelijke inzichten en waardes gevonden bij onze doelgroep, dus hier willen we verder mee aan de slag gaan in kwartaal 6.

## Leerdoelen voor team:
- Deadlines in de gaten houden.
- Een strakke taakverdeling maken.
- Een teamleider hebben die weet welke rol hij wil vervullen in het team.
- Je eigen kwalteiten delen.
- Teamgenoten meer helpen met je eigen kwaliteiten.
- Bepalen wat de waarde is van bepaalde uitkomsten (bijv. onderzoek).
- Waardes van de doelgroep uitzoeken en vasthouden!
- Volledig verplaatsen in doelgroep.

## Leerdoelen voor mij:
- Werk uit handen geven.
- Een vaste ontwerpstijl maken.
- Experimenteren en durven.
- Leren wanneer je wel en niet perfectionistisch moet zijn.
- Leiderrol afbakenen.
