---
title: Design Challenge 2 - Reflectie
date: 2018-01-18
---

Kwartaal 2 was weer een leerzaam kwartaal! Ik heb nieuwe competenties geleerd (onder andere verbeelden & uitwerken en ontwerpresultaten evalueren). Ook heb ik nieuwe beroepsproducten leren maken. Kwartaal 2 zag er heel anders uit dan kwartaal 1. Alles ging veel sneller en makkelijker, omdat ik met sommige opdrachten al ervaring had door het eerste kwartaal. Ik merkte dus zeker enige vooruitgang. Dit kwartaal heb ik meer leren professionaliseren, samenwerken en om een ontwerpproces in te richten. Ook heb ik geleerd om te onderzoeken, ontwerpresultaten te evalueren en om te verbeelden en uit te werken. Ik heb veel feedback gehad, die ik ook zeker wil verwerken in kwartaal 3. Ik ben benieuwd wat kwartaal 3 voor mij te bieden heeft!
